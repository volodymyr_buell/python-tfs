#!/usr/bin/env python

import ConfigParser
import tfslib
import os


def get_item():
    for itemId in args.workItemId:
        result = client.get_work_item(int(itemId), True)

        import pprint

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(result)

        #import json
        #
        #print json.dumps(result, indent=4)


def get_item_revision():
    for itemId in args.workItemId:
        result = client.get_work_item(int(itemId), True)

        revision = result[0].data_rows[0]['System.Rev']
        print revision


def list_queries():
    print 'List Queries'
    for itemId in args.workItemId:
        result = client.get_stored_queries(int(itemId))

        print result


def do_query():
    print 'Do query'
    import wiql_parser

    converter = wiql_parser.Converter()
    xml = converter.convert('stub')

    result = client.query_work_items(xml)


def do_update():
    for itemId in args.workItemId:
        result = client.get_work_item(int(itemId), True)
        revId = result[0].data_rows[0]['System.Rev']

        result = client.update_work_item(itemId, revId, {'Numara.Branch': 'Updated'})
        print result


command_map = {
    'get': get_item,
    'get_revision': get_item_revision,
    'lq': list_queries,
    'q': do_query,
    'update': do_update
}


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.INFO)

    from argparse import ArgumentParser

    parser = ArgumentParser(description='Simple TFS console-line client.')
    parser.add_argument(dest='command', metavar='command', type=str, help='work item ID')
    parser.add_argument(dest='workItemId', metavar='workItemId', type=int, nargs='+', help='work item ID')
    parser.add_argument('--verbose', '-v', action='count')
    parser.add_argument('-c', '--use_color', dest='use_color', action='store_true', help='Output format: [ansi, ascii]')
    args = parser.parse_args()

    # if args.use_color:
    #     fmt = AnsiFormatter()
    # else:
    #     fmt = BaseFormatter()

    config = ConfigParser.ConfigParser()
    config.read(os.path.expanduser('~/.tfsclientrc'))

    login = config.get('user', 'login')
    password = config.get('user', 'password')
    tfs_url = config.get('tfs', 'url')

    client = tfslib.TfsClient(tfs_url, login, password)
    # result = client.queryWorkItems('Select ID, Title from WorkItems where (State = \'Active\') order by Title')

    # print args

    command_map[args.command]()
