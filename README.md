# python-tfs

python-tfs is a pure python library ans simple console-line client that provides limited access to Microsoft TFS work items.

## Features

 * Reading TFS work item by item ID
 * Modifying TFS work item
 * Simple console-line client to test
 
## Requirements

 * Python >= 2.6, but < 3.0 (porting to 3.0 is in progress)
 * SUDS

## Usage

The good starting point is to read the sample client that shipped with the library "tfs.py".

The simplified code for getting a TFS ticket would be:

```python
import tsflib

client = tfslib.TfsClient(tfs_url, login, password)
tfs_item = client.get_work_item(itemId, True)
print tfs_item
```

## Configuring console-line client

To work with console-line client you need to specify all needed information in ~/.tfsclient file.


.tfsclientrc

```
[user]
login = domain\user
password = yourpassword

[tfs]
url = http://your_tfs_server:8080/tfs
```

After that just use `tfs.py get <work_item_id>` to dump an item to stdout.